Homework assignment no. 3, Superheroes and villains
====================================

**Publication date:**  May 6th, 2024

**Submission deadline:** May 20th, 2024

CHANGELOG
-------------------

General information
-------------------
This section provides general information about the initial structure of the provided codebase.

### Project Structure

The structure of the project provided as a base for your implementation should meet the following criteria.

1. Package ```cz.muni.fi.pb162.hw03``` contains classes and interfaces provided as a part of the assignment.

- **Do not modify or add any classes or subpackages into this package.**

2. Package  ```cz.muni.fi.pb162.hw03.impl``` should contain your implementation.

- **Anything outside this package will be ignored during evaluation.**

Additionally, the following applies for the initial contents of ``cz.muni.fi.pb162.hw03.impl``

1) The information in **javadoc has precedence over everything**
2) **Interfaces** are advised to be implemented
3) Otherwise, you can modify the code (unless tests are affected) as you see fit
4) When in doubt, **ASK**

**Note:**  
*While a modification of the interface is not strictly prohibited, you don't want to end
with [god object](https://en.wikipedia.org/wiki/God_object) implementations.    
On the other hand, you want to adhere to
the [single responsibility principle](https://en.wikipedia.org/wiki/Single-responsibility_principle).

### Names in This Document

Unless fully classified name is provided, all class names are relative to the package ```cz.muni.fi.pb162.hw03```
or ```cz.muni.fi.pb162.hw03.impl``` for classes implemented as a part of your solution.

### Compiling the Project

The project can be compiled and packaged in the same way you already know.

```bash
$ mvn clean install
```

The only difference is that unlike the seminar project, the checks for missing documentation and a style violation will
produce an error this time.
You can disable this behavior temporarily when running this command.

```bash
$ mvn clean install -Dcheckstyle.skip
```

You can consult your seminar teacher to help you set the ```checkstyle.skip``` property in your IDE (or just google it).

### Submitting the Assignment

Follow instructions of your tutor because the procedure to submit your solution may differ based on your seminar group.
However, there are two ways of submission in general:

* Fork the project, develop your code in a development branch, and finally ask for the merge.
* Submit ```target/homework03-2024-1.0-SNAPSHOT-sources.jar``` to the homework vault.

### Test Execution Note
As described further in this assignment, the application will create a [file](#fight-history-file). The location of this file will be configurable, however by default the file is located in the **current working directory**. The maven configuration sets the working directory for test execution to the `target` directory, so the file is deleted or created as needed there. However, when tests are executed from the IDE the working directory might be different -- in case of IntelliJ IDEA the working directory will be the project root. 

### Minimal Requirements for Acceptance

- Fulfilling all Java course standards (documentation, conventions, etc.)
- Proper code decomposition
    - Split your code into multiple classes
    - Organize your classes in packages
- Single responsibility
    - Class should ideally have a single purpose
- Implementation must adhere to the defined CLI!
- All files will use `UTF-8` encoding!

Assignment Description
-------------
In this assignment you will implement a game-like CLI application simulating fights between superheroes and villains.
This time the goals of this exercise are:

- Get familiar with file IO in Java
- Understand the difference between classpath and external files/resources.
- Understanding of existing code
    - The assignment skeleton contains a bit more code
    - Except for the `Applation*` classes you don't have to use it (it will make things easier though)
- Understanding of 3rd party code available as a library
    - See the `net.cechacek.edu:pb162-csv-parser` dependency in `pom.xml`
    - More information (including javadocs) is available at [Github](https://github.com/jcechace/pb162-csv-parser/)
    - Usage is completely optional (if you want to deal with CSV parsing yourself, that is)
- Proper code decomposition
    - A bunch of classes and interfaces is provided as part of this assignment
    - You don't have to use them, however building your application from their respective implementations will likely
      lead to a passable solution :).

The project compiles into single executable self-contained `super.jar` JAR file. Feel free to look into `pom.xml` to how
this is achieved. More importantly though the provided project skeleton is a CLI application.

### Input, Source and Output Data

The application will deal with 3 distinct types of data

- **Input**: Data input by the user via the [CLI](#application-command-line-interface)
- **Source**: Persistent data used by the application,
    - character data from the provided `/heroes.csv`
      and `/villains.csv` [resource files](#a-note-on-classpath-resources-vs-external-files)
    - fight history data from the  [fight history file](#fight-history-file)
- **Output**: Data which are either
    - printed to the standard output
    - written to the [fight history file](#fight-history-file)

### Fight History File

The fight history file will be a regular text file where the application persists the history of fights between
application runs. The following information should be stored in this file:

- Total number of fights
- Number of fights won by heroes
- Number of fights won by villains
- The result of the last executed fight
    - winning team
    - winner id
    - winner name
    - loser id
    - loser name

The first three top-level bullets can be conveniently represented by the `model.FightStats` record and similarly the
last can be represented by the `model.FightResult` record. A combination of the two can then be expressed via
the `model.FightHistory` record.

The assignment places no format restrictions on the file. The only requirement is that it must be a `UTF-8` encoded text
file understandable by humans.

### Application Command Line Interface

The application will provide an easy-to-use command line interface with the options and commands described bellow. You
don't have to write the CLI yourself -- it uses a library called [JCommander](https://jcommander.org/) and the code is
already provided as part of the project skeleton. You are however encouraged to study the code as it might be helpful
for your future Java projects. Refer to the [usage](#example-usage) section in order to understand how the CLI is
supposed to be used.

#### Main Application Options

Top-level application options controlling the behaviour across all commands. The following options are supported:

| CLI option (long) | CLI option (short) | Arguments | Default             | Required | Description             |
|-------------------|--------------------|-----------|---------------------|----------|-------------------------|
| --help            |                    | -         | false (if not used) | false    | Print application usage |
| --file            | -f                 | String    | super-history       | false    | Fight history file      |

#### Query Command

Queries the character data.

Executed via `java -jar super.jar [app options] query [command options] <id>`

Command options:

| CLI option (long) | CLI option (short) | Arguments        | Default | Required | Description      |
|-------------------|--------------------|------------------|---------|----------|------------------|
| _not named_       | _not named_        | long             |         | true     | Character \<id\> |
| --team            | -t                 | heroes\|villains | heroes  | false    | Character team   |

#### Count Command

Counts the characters.

Executed via `java -jar super.jar [app options] count [command options]`

Command options:

| CLI option (long) | CLI option (short) | Arguments        | Default | Required | Description    |
|-------------------|--------------------|------------------|---------|----------|----------------|
| --team            | -t                 | heroes\|villains | heroes  | false    | Character team |

#### Fight Command

Executes a fight between random hero and villain.

Executed via `java -jar super.jar [app options] fight [command options]`

Command options:

| CLI option (long) | CLI option (short) | Arguments | Default             | Required | Description             |
|-------------------|--------------------|-----------|---------------------|----------|-------------------------|
| --no-history      | -n                 | -         | false (if not used) | false    | Don't record this fight |

#### History Command

Displays fight history from the [fight history file](#fight-history-file).

Executed via `java -jar super.jar [app options] history [command options]`

Command options:

| CLI option (long) | CLI option (short) | Arguments | Default             | Required | Description          |
|-------------------|--------------------|-----------|---------------------|----------|----------------------|
| --reset           | -r                 | -         | false (if not used) | false    | Resets fight history |

_Note: the command must work regardless of the history file existence._

### Example usage

The following examples showcase the expected outputs when calling the application in accordance with the CLI described
above

Execute help (this comes out of the box, so the output is truncated)

```bash
$ java -jar super.jar --help
Usage: Application [options] [command] [command options]
```

Execute a fight between random hero and villain (record result into default history file)

```bash
$ java -jar super.jar fight
The fighters
===
Team: HEROES
Id: #792
Name: Mbaku (MCU)
Alias: MBaku
Level: 4
Powers: Agility,  Durability,  Marksmanship,  Peak Human Condition,  Reflexes,  Stamina,  Weapon-based Powers,  Weapons Master

Team: VILLAINS
Id: #288
Name: Lex Luthor (DCEU)
Alias: Alexander Joseph Luthor
Level: 9
Powers: Intelligence,  Intuitive aptitude,  Marksmanship
---

The winner is Lex Luthor (DCEU) (#288)

# Display contents of the default history file (actual output obfuscated as the format is not important)
$ cat super-history
abc_super-history
```

Execute a fight between random hero and villain (record result into fights.txt)

```bash
$ java -jar super.jar -f fights.txt fight
The fighters
===
Team: HEROES
Id: #257
Name: Annabeth Chase (FOX)
Alias: Annabeth Chase
Level: 6
Powers: Intelligence,  Weapon-based Powers,  Weapons Master

Team: VILLAINS
Id: #68
Name: KGBeast
Alias: Anatoli Knyazev
Level: 4
Powers: Agility,  Intelligence,  Invulnerability,  Marksmanship,  Stamina,  Stealth,  Weapon-based Powers,  Weapons Master
---

The winner is Annabeth Chase (FOX) (#257)

# Display contents of the default history file (actual output obfuscated as the format is not important)
# Remains unchanged
$ cat super-history
abc_super-history

# Display contents of the fights.txt file (actual output obfuscated as the format is not important)
$ cat super-history
abc_fights_txt
```

Execute a fight between random hero and villain (don't record result)

```bash
$ java -jar super.jar fight --no-history
The fighters
===
Team: HEROES
Id: #143
Name: Bucky Barnes (MCU)
Alias: Bucky Barnes
Level: 3
Powers: Accelerated Healing,  Agility,  Durability,  Marksmanship,  Reflexes,  Stamina,  Stealth,  Super Speed,  Super Strength,  Weapons Master

Team: VILLAINS
Id: #176
Name: Leatherface
Alias: Jed Sawyer
Level: 7
Powers: Reflexes
---

The winner is Leatherface (#176)

# Display contents of the default history file (actual output obfuscated as the format is not important)
# Remains unchanged
$ cat super-history
abc_super-history

# Display contents of the fights.txt file (actual output obfuscated as the format is not important)
# Remains unchanged
$ cat super-history
abc_fights_txt
```

Query for hero with id 42

```bash
$ java -jar super.jar query -t heroes 42
Team: HEROES
Id: #42
Name: Wonder Woman (Smallville)
Alias: Diana Prince
Level: 15
Powers: Accelerated Healing,  Animal Oriented Powers,  Cold Resistance,  Dexterity,  Dimensional Travel,  Fire Resistance,  Flight,  Heat Resistance,  Invulnerability,  Super Speed,  Super Strength
```

Query for villain with id 42

```bash
$ java -jar super.jar query -t villains 42
Team: VILLAINS
Id: #42
Name: King Shark
Alias: Nanaue
Level: 8
Powers: Animal Attributes,  Animal Oriented Powers,  Cold Resistance,  Durability,  Enhanced Sight,  Enhanced Smell,  Invulnerability,  Longevity,  Natural Armor,  Natural Weapons,  Radar Sense,  Regeneration,  Stamina,  Stealth,  Sub-Mariner,  Super Speed,  Super Strength,  Underwater breathing
```

Count heroes

```bash
$ java -jar super.jar count -t heroes
941
```

Count villains

```bash
$ java -jar super.jar count -t villains  
570
```

Display fight history (assuming few more fights happened since the first fight example)

```bash
$ java -jar super.jar history 
Fight statistics
===
Number of fights: 4
Heroes won: 2
Villains won: 2
---

Last recorded fight
===
Winning Team: HEROES
Winner: Hellstorm (#637)
Loser: Cull Obsidian (MCU) (#259)
```

Reset fight history

```bash
$ java -jar super.jar history -r
Fight history removed!

# Try to display contents fo the history file (which was removed)
$ cat super-history
cat: super-history: No such file or directory
```

Display fight history if history file doesn't exist (no fights happened)

```bash
$ java -jar super.jar history   
Fight statistics
===
Number of fights: 0
Heroes won: 0
Villains won: 0
---

Last recorded fight
===
No fight recorded yet
```

Display fight history from the other file

```bash
$ java -jar super.jar -f fights.txt  history
Fight statistics
===
Number of fights: 1
Heroes won: 1
Villains won: 0
---

Last recorded fight
===
Winning Team: HEROES
Winner: Annabeth Chase (FOX) (#257)
Loser: KGBeast (#68)
```

Implementation notes
-----------
As already mentioned several times, the provided project skeleton is a bit more complex this time. Besides the goal of
being able to navigate and understand an existing code, the command line interface needed to be also written. Thus, you
might struggle to identify where to begin. Let us have a brief look at the `impl.app` package.

**TLDR\; The entry point for the top-level application logic is `Application#run()` method**

From the application execution point the main class is `app.ApplicationRunner` as it contains
the `public static void main(String[] args)` method. The implementation is rather simple and it does the following

- Initializes the CLI
- Parses the command line arguments used to execute the application
- Creates an instance the `Application` class
- Executes the `Application#run()` method

The `app.Application` class then represent the logical application.

- Constructor initializes all dependencies
- The `run()` method then represents the application logic which gets executed
    - think of it as the actual `main` method of your application from runtime point of view.

### A Note on Classpath Resources vs External Files

A classpath resource is in general any file which exist on the java classpath during program runtime. Such resource can
be
added to the classpath when running a java application, or it could be embedded in jar file. The latter is the case for
our `/heroes.csv` and `/villains.csv` files. In case of maven, teh default path for these resources is
in `src/main/resources` or `src/test/resources` respectively.

Getting a path of external file is trivial, you just call `Paths.get("/some/path/to/file.txt")`. In case of resource
files the situation is a bit more complicated -- as you don't know the location. Fortunately java provides you with an
API to get hold of such resource. The following code demonstrates how one would access the `heroes.csv` resource inside
the
`Application.run()` method.

```java
/*
 * Notice the use of absolute path starting with slash.
 * Otherwise, JVM would try locating the file as relative to the current class
 */

// Get resource as URL
URL resourceUrl = getClass().getResource("/heroes.csv");

// Get resource as INPUT STREAM
InputStream resourceIs = getClass().getResourceAsStream("/heroes.csv");
```

An attentive student might notice that there is another factory method on `Paths` class, specifically
the `Paths.get(URI uri)`
and they may try to use that to get hold of the `Path` object for given resource like
this `Paths.get(resourceUrl.toURI())`.
This will work when running tests or when you run your application from the IDE. The problem comes when you try to run
your
application as executable JAR. Since JARs are basically just ZIP archives with additional metadata, it means that they
would
be creating a filesystem path to a file inside a zip archive. This will lead to problems with uninitialised zip
filesystem.

Without going
into [details](https://stackoverflow.com/questions/25032716/getting-filesystemnotfoundexception-from-zipfilesystemprovider-when-creating-a-p),
the general advice
for the purposes of our course is to **always access embedded class path resource via `Class.getResourceAsStream()`**.

### A Note on Data Management

Provided CSV library offers a nice way of reading domain data from CSV files. In the most straightforward form it can
read each CSV entry either as `List<String>` or as `Map<String, String>` in case of CSV file with header. Additionally,
a more advanced approach is available -- by providing an implementation of `net.cechacek.edu.pb162.csv.PlainValueConvertor<D>` or `net.cechacek.edu.pb162.csv.HeadedValueConvertor<D>`
the raw CSV entry type can be converted into a domain object.

Last but not least the `cz.muni.fi.pb162.hw03.impl.model` package contains domain model records representing the data
needed by your application. Similarly, the `cz.muni.fi.pb162.hw03.impl.business` package contains interfaces defining
the business logic (the actual behaviour of various parts of your application). Both are likely to be useful for your
implementation, however you are free to do with them as you like.

Acknowledgement
---
The author of this assignment would herby like to tank the authors of the [Quarkus Super Heroes](https://quarkus.io/quarkus-workshops/super-heroes/spine.html) workshop, which is an amazing  learning material for the supersonic subatomic java framework [Quarkus](https://quarkus.io/). 

The topic and the data for this assignment were shamelessly "borrowed" from this project in the spirit of [open source](https://en.wikipedia.org/wiki/Open-source_software). Thank you guys!

_Note: Quarkus Super Heroes workshop as well as the Quarkus framework are distributed under the [Apache 2.0 license](https://www.apache.org/licenses/LICENSE-2.0) which permits redistribution, provided the conditions stated in the license are met._