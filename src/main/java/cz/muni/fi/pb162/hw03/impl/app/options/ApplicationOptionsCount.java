package cz.muni.fi.pb162.hw03.impl.app.options;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import cz.muni.fi.pb162.hw03.cmd.NamedCommand;
import cz.muni.fi.pb162.hw03.cmd.TeamConverter;
import cz.muni.fi.pb162.hw03.impl.model.Team;

/**
 * Command line options (flags) for Library application
 */
@SuppressWarnings({ "FieldMayBeFinal", "FieldCanBeLocal" })
@Parameters(commandDescription = "Counts characters")
public final class ApplicationOptionsCount implements NamedCommand {

    /**
     * Name of this command
     */
    public static final String NAME = "count";

    @Parameter(
            names = { "-t", "--team" }, description = "Character team [heroes|villains]",
            converter = TeamConverter.class
    )
    private Team team = Team.HEROES;

    public Team getTeam() {
        return team;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
