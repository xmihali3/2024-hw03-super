package cz.muni.fi.pb162.hw03.impl.app.options;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import cz.muni.fi.pb162.hw03.cmd.NamedCommand;

/**
 * Command line options (flags) for Library application
 */
@SuppressWarnings({ "FieldMayBeFinal", "FieldCanBeLocal" })
@Parameters(commandDescription = "Orchestrates a fight between random superhero and villain")
public final class ApplicationOptionsFight implements NamedCommand {

    /**
     * Name of this command
     */
    public static final String NAME = "fight";

    @Parameter(names = { "-n", "--no-history"}, description = "Don't record fight in history")
    private boolean noHistory = false;


    public boolean isNoHistory() {
        return noHistory;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
