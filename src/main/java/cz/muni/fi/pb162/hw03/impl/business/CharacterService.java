package cz.muni.fi.pb162.hw03.impl.business;

import cz.muni.fi.pb162.hw03.impl.model.SuperCharacter;

import java.util.Optional;

/**
 * Provides access to characters
 */
public interface CharacterService {

    /**
     * Find character by given id
     *
     * @param key id identifier
     * @return character with given id or empty optional if not found
     */
    Optional<SuperCharacter> findCharacter(Long key);

    /**
     * Gets random character
     * @return random character
     */
    SuperCharacter getRandomCharacter();

    /**
     * @return total number of characters
     */
    long count();
}   
