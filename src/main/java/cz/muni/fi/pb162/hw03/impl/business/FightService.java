package cz.muni.fi.pb162.hw03.impl.business;

import cz.muni.fi.pb162.hw03.impl.model.SuperCharacter;
import cz.muni.fi.pb162.hw03.impl.model.FightResult;

/**
 * Orchestrates fight between two characters
 */
public interface FightService {

    /**
     * Orchestrates fight between a hero and a villain.
     * <p>
     *     For simplicity the character with higher level wins.
     *     In case of a draw, the hero wins of course!
     * </p>
     *
     * @param hero the fighting hero
     * @param villain the fighting villain
     * @throws IllegalArgumentException if hero is not a hero or villain is not a villain
     * @return result of this fight
     */
    FightResult fight(SuperCharacter hero, SuperCharacter villain);
}
