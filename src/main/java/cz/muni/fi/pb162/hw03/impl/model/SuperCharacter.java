package cz.muni.fi.pb162.hw03.impl.model;

import java.util.List;
import java.util.Objects;

/**
 * Character representation
 * <p>
 *     Use of this class is not mandatory, however its {@link #toString()} matches the mandatory CLI output
 * </p>
 *
 * @param id character id
 * @param team team (heroes or villains)
 * @param name name of the character
 * @param alias other name of the character
 * @param level level of the character
 * @param powers powers of the character
 */
public record SuperCharacter(
        Long id,
        Team team,
        String name,
        String alias,
        Integer level,
        List<String> powers) {
    /**
     * Validating compact constructor
     */
    public SuperCharacter {
        Objects.requireNonNull(id);
        Objects.requireNonNull(team);
        Objects.requireNonNull(name);
        Objects.requireNonNull(alias);
        Objects.requireNonNull(level);
        Objects.requireNonNull(powers);
    }

    @Override
    public String toString() {
        return """
                Team: %s
                Id: #%d
                Name: %s
                Alias: %s
                Level: %d
                Powers: %s
                """.formatted(team, id, name, alias, level, String.join(", ", powers));
    }
}
