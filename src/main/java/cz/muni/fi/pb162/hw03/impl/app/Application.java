package cz.muni.fi.pb162.hw03.impl.app;

import cz.muni.fi.pb162.hw03.cmd.CommandLine;
import cz.muni.fi.pb162.hw03.impl.app.options.ApplicationOptions;
import cz.muni.fi.pb162.hw03.impl.app.options.ApplicationOptionsCount;
import cz.muni.fi.pb162.hw03.impl.app.options.ApplicationOptionsFight;
import cz.muni.fi.pb162.hw03.impl.app.options.ApplicationOptionsQuery;
import cz.muni.fi.pb162.hw03.impl.app.options.ApplicationOptionsHistory;

import java.util.Objects;
import java.util.Optional;


/**
 * Represents logical application
 */
@SuppressWarnings({"OptionalUsedAsFieldOrParameterType", "FieldCanBeLocal"})
final class Application {

    // Likely don't want to touch from here
    // > These can ve used to detect top-level/command CLI options
    // > as well as detecting which command to run
    private final CommandLine cli;
    private final ApplicationOptions options;
    private final Optional<ApplicationOptionsFight> optionsFight;
    private final Optional<ApplicationOptionsQuery> optionsQuery;
    private final Optional<ApplicationOptionsHistory> optionsHistory;
    private final Optional<ApplicationOptionsCount> optionsCount;
    // to here


    /**
     * Creates the application
     *
     * @param options top-level command line options
     * @param cli command line interface object
     */
    Application(ApplicationOptions options, CommandLine cli) {
        Objects.requireNonNull(options);
        Objects.requireNonNull(cli);
        this.cli = cli;
        this.options = options;
        // > Here we get the command options from the CLI
        // only one of these will be populated, rest will be empty
        this.optionsFight = cli.getOptions(ApplicationOptionsFight.class);
        this.optionsQuery = cli.getOptions(ApplicationOptionsQuery.class);
        this.optionsHistory = cli.getOptions(ApplicationOptionsHistory.class);
        this.optionsCount = cli.getOptions(ApplicationOptionsCount.class);
    }

    /**
     * Note:    This method represents the runtime logic.
     * However, you should still use proper decomposition.
     * <p>
     * Application runtime logic
     */
    void run() {
        throw new UnsupportedOperationException("Implement this method");
    }
}
