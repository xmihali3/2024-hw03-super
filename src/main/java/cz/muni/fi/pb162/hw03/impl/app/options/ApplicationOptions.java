package cz.muni.fi.pb162.hw03.impl.app.options;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.converters.PathConverter;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Command line options (flags) for Library application
 */
@SuppressWarnings({ "FieldMayBeFinal", "FieldCanBeLocal" })
public final class ApplicationOptions {

    @Parameter(names = "--help", help = true)
    private Boolean showUsage = false;

    @Parameter(
            names = { "-f", "--file" }, description = "Fight statistics file",
            converter = PathConverter.class
    )
    private Path statsFile = Paths.get("super-history");

    public boolean isShowUsage() {
        return showUsage;
    }

    public Path getStatsFile() {
        return statsFile;
    }
}
