package cz.muni.fi.pb162.hw03.cmd;

/**
 * CLI command with name
 */
public interface NamedCommand {

    /**
     * @return name of this command
     */
    String getName();
}
