package cz.muni.fi.pb162.hw03.cmd;

import com.beust.jcommander.IStringConverter;
import cz.muni.fi.pb162.hw03.impl.model.Team;

public class TeamConverter implements IStringConverter<Team> {
    @Override
    public Team convert(String value) {
        return Team.valueOf(value.toUpperCase());
    }
}
