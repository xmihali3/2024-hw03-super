package cz.muni.fi.pb162.hw03.impl.app.options;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import cz.muni.fi.pb162.hw03.cmd.NamedCommand;

/**
 * Command line options (flags) for Library application
 */
@SuppressWarnings({ "FieldMayBeFinal", "FieldCanBeLocal" })
@Parameters(commandDescription = "Displays fight history")
public final class ApplicationOptionsHistory implements NamedCommand {

    /**
     * Name of this command
     */
    public static final String NAME = "history";

    @Parameter(names = {"-r", "--reset"}, description = "Resets fight statistics")
    private boolean reset = false;

    public boolean isReset() {
        return reset;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
