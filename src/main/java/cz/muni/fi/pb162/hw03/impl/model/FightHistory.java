package cz.muni.fi.pb162.hw03.impl.model;

/**
 * Combined data for fight history
 * @param fightStats
 * @param lastFightResult
 */
public record FightHistory(
        FightStats fightStats,
        FightResult lastFightResult
) {
    /**
     * Creates empty history
     */
    public FightHistory() {
        this(new FightStats(0L, 0L, 0L), null);
    }
}
