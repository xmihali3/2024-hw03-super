package cz.muni.fi.pb162.hw03.impl.business;

import cz.muni.fi.pb162.hw03.impl.model.FightResult;
import cz.muni.fi.pb162.hw03.impl.model.FightStats;

import java.util.Optional;

/**
 * Operations over fight history
 */
public interface HistoryService {


    /**
     * Access fight statistics
     *
     * @return fight statistics
     */
    FightStats getFightStats();

    /**
     * Records fight to history
     *
     * @param fightResult result of a fight
     */
    void saveFightResult(FightResult fightResult);

    /**
     * Access last recorded fight
     *
     * @return last fight
     */
    Optional<FightResult> getLastFightResult();

    /**
     * Resets fight statistics
     */
    void reset();
}
