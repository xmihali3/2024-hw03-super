package cz.muni.fi.pb162.hw03.impl.model;

import java.util.Objects;

/**
 * <p>
 * Use of this class is not mandatory, however its {@link #toString()} matches the mandatory CLI output
 * </p>
 *
 * @param winningTeam team that won the fight
 * @param winnerId    winner's id
 * @param winnerName  winner's name
 * @param loserId     loser's id
 * @param loserName   loser's name
 */
public record FightResult(
        Team winningTeam,
        Long winnerId,
        String winnerName,
        Long loserId,
        String loserName) {

    /**
     * Validating compact constructor
     */
    public FightResult {
        Objects.requireNonNull(winningTeam);
        Objects.requireNonNull(winnerId);
        Objects.requireNonNull(winnerName);
        Objects.requireNonNull(loserId);
        Objects.requireNonNull(loserName);
    }

    @Override
    public String toString() {
        return """
                Winning Team: %s
                Winner: %s (#%d)
                Loser: %s (#%d)
                """.formatted(winningTeam, winnerName, winnerId, loserName, loserId);
    }
}
