package cz.muni.fi.pb162.hw03.impl.model;

import java.util.Objects;

/**
 * Fight statistics
 *
 * <p>
 *     Use of this class is not mandatory, however its {@link #toString()} matches the mandatory CLI output
 * </p>
 *
 * @param fightCount total number of recorded fights
 * @param heroWinsCount number of recorded fights won by heroes
 * @param villainWinsCount number of recorded fights won by villains
// * @param lastFightResult last recorded fight
 */
public record FightStats(
        Long fightCount,
        Long heroWinsCount,
        Long villainWinsCount) {

    /**
     * Validating compact constructor
     */
    public FightStats {
        Objects.requireNonNull(fightCount);
        Objects.requireNonNull(heroWinsCount);
        Objects.requireNonNull(villainWinsCount);
    }

    @Override
    public String toString() {
        return """
                Number of fights: %d
                Heroes won: %d
                Villains won: %d
                """.formatted(fightCount, heroWinsCount, villainWinsCount);
    }
}
