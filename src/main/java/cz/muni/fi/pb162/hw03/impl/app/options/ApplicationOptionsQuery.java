package cz.muni.fi.pb162.hw03.impl.app.options;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.converters.LongConverter;
import cz.muni.fi.pb162.hw03.cmd.NamedCommand;
import cz.muni.fi.pb162.hw03.cmd.TeamConverter;
import cz.muni.fi.pb162.hw03.impl.model.Team;

/**
 * Command line options (flags) for Library application
 */
@SuppressWarnings({ "FieldMayBeFinal", "FieldCanBeLocal" })
@Parameters(commandDescription = "Queries characters")
public final class ApplicationOptionsQuery implements NamedCommand {

    /**
     * Name of this command
     */
    public static final String NAME = "query";

    @Parameter(description = "Character id", required = true, converter = LongConverter.class)
    private Long id;

    @Parameter(
            names = { "-t", "--team" }, description = "Character team [heroes|villains]",
            converter = TeamConverter.class
    )
    private Team team = Team.HEROES;

    public Team getTeam() {
        return team;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
