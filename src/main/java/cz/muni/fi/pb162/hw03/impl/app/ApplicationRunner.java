package cz.muni.fi.pb162.hw03.impl.app;

import cz.muni.fi.pb162.hw03.cmd.CommandLine;
import cz.muni.fi.pb162.hw03.cmd.CommandLineException;
import cz.muni.fi.pb162.hw03.impl.app.options.ApplicationOptions;
import cz.muni.fi.pb162.hw03.impl.app.options.ApplicationOptionsCount;
import cz.muni.fi.pb162.hw03.impl.app.options.ApplicationOptionsFight;
import cz.muni.fi.pb162.hw03.impl.app.options.ApplicationOptionsQuery;
import cz.muni.fi.pb162.hw03.impl.app.options.ApplicationOptionsHistory;

import java.util.List;

/**
 * Note:    While it is possible to modify this class,
 *          it is unlikely that one would need to do so.
 *
 * CLI bootstrapping class for Application
 */
public class ApplicationRunner {
    /**
     * Application entry point
     *
     * @param args command line arguments of the application
     */
    public static void main(String[] args) {
        var options = new ApplicationOptions();
        var characterCommand = new ApplicationOptionsQuery();
        var fightCommand = new ApplicationOptionsFight();
        var statsCommand = new ApplicationOptionsHistory();
        var countCommand = new ApplicationOptionsCount();

        var cli = new CommandLine(
                Application.class.getSimpleName(),
                options,
                List.of(characterCommand, fightCommand, statsCommand, countCommand));

        try {
            cli.parseArguments(args);

            if (options.isShowUsage()) {
                cli.printUsage();
                return;
            }

            var application = new Application(options, cli);

            application.run();

        } catch (CommandLineException ex) {
            System.err.println("Error: " + ex.getMessage());
            cli.printUsage();
        } catch (Exception ex) {
            System.err.println("Error: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
}
