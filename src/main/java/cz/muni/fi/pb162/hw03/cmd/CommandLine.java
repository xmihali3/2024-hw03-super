package cz.muni.fi.pb162.hw03.cmd;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Command Line Interface
 */
public final class CommandLine {

    private final JCommander commander;

    private final List<NamedCommand> commands = new ArrayList<>();

    /**
     * Constructs new CLI instance
     *
     * @param programName name of the program
     * @param options application options to be populated with arguments
     * @param commandMap application command objects to be populated with arguments
     */
    public CommandLine(String programName,
                       Object options,
                       List<NamedCommand> commandMap) {
        var builder  = JCommander.newBuilder().addObject(options);

        commandMap.forEach(command -> {
            builder.addCommand(command.getName(), command);
            commands.add(command);
        });

        commander = builder.build();
        commander.setProgramName(programName);
    }

    /**
     * Parses command line arguments (terminates on error)
     *
     * @param args command line arguments of the application
     */
    public void parseArguments(String[] args) {
        try {
            commander.parse(args);
        } catch (ParameterException e) {
            throw new CommandLineException(e.getMessage(), e);
        }
    }

    /**
     * Shows usage of the application
     */
    public void printUsage() {
        commander.usage();
    }

    /**
     * Gets command options
     * @param commandType command class
     * @return command options or empty optional if the command was not invoked
     * @param <T> type of command
     */
    public <T extends NamedCommand> Optional<T> getOptions(Class<T> commandType) {
        return commands.stream()
                .filter(c -> c.getClass() == commandType)
                .filter(c -> c.getName().equals(commander.getParsedCommand()))
                .map(commandType::cast)
                .findFirst();
    }
}
