package cz.muni.fi.pb162.hw03.impl.model;

/**
 * Teams
 */
public enum Team {
    HEROES,
    VILLAINS
}
