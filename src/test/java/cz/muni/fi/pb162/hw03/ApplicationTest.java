package cz.muni.fi.pb162.hw03;

import com.github.stefanbirkner.systemlambda.SystemLambda;
import cz.muni.fi.pb162.hw03.impl.app.ApplicationRunner;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.junit.jupiter.InjectSoftAssertions;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

@ExtendWith(SoftAssertionsExtension.class)
public class ApplicationTest {

    public static final List<Map<String, String>> HEROES = List.of(
            hero(1L, "Hero1", "AliasH1", 1, "foo, bar"),
            hero(2L, "Hero2", "AliasH2", 2, "bar, baz"),
            hero(3L, "Hero3", "AliasH3", 3, "baz, qux")
    );

    public static final List<Map<String, String>> VILLAINS = List.of(
            villain(1L, "Villain1", "AliasV1", 3, "baz, qux"),
            villain(2L, "Villain2", "AliasV2", 1, "foo, bar"),
            villain(3L, "Villain3", "AliasV3", 2, "bar, baz")
    );

    public static Map<String, String> character(String team, Long id, String name, String alias, Integer level, String powers) {
        return Map.of(
                "team", team,
                "id", id.toString(),
                "name", name,
                "alias", alias,
                "level", level.toString(),
                "powers", powers
        );
    }

    @InjectSoftAssertions
    SoftAssertions softly;

    @BeforeEach
    void resetHistory() throws IOException {
        // Delete the history file in current workdir
        Files.deleteIfExists(Path.of("super-history"));
    }

    @Test
    void queryHeroes() throws Exception {
        var output = runApp("query", "-t", "heroes", "2");
        var expected = expectedQuery(2, HEROES);
        Assertions.assertThat(output).isEqualToNormalizingWhitespace(expected);
    }

    @Test
    void queryVillains() throws Exception {
        var output = runApp("query", "-t", "villains", "3");
        var expected = expectedQuery(3, VILLAINS);
        Assertions.assertThat(output).isEqualToNormalizingWhitespace(expected);
    }

    @Test
    void countHeroes() throws Exception {
        var output = runApp("count", "-t", "heroes");
        Assertions.assertThat(output).isEqualToNormalizingWhitespace("3");
    }

    @Test
    void countVillains() throws Exception {
        var output = runApp("count", "-t", "villains");
        Assertions.assertThat(output).isEqualToNormalizingWhitespace("3");
    }

    @Test
    void displayDefaultHistoryNoFight() throws Exception {
        var output = runApp("history");
        var expected = expectedEmptyHistory();
        Assertions.assertThat(output).isEqualToNormalizingWhitespace(expected);
    }

    @Test
    void fightAndRecordDefaultHistory() throws Exception {
        var fightOutput = runApp("fight");
        var ids = parseCharacterIds(fightOutput);
        var hero = HEROES.get(ids.get(0) - 1);
        var villain = VILLAINS.get(ids.get(1) - 1);
        var winner = winner(hero, villain);
        var loser = loser(hero, villain);
        var fightExpected = expectedFightResult(hero, villain, winner);

        Assertions.assertThat(fightOutput).isEqualToNormalizingWhitespace(fightExpected);

        var historyOutput = runApp("history");
        var historyExpected = expectedHistory(
                1,
                winner.equals(hero) ? 1 : 0,
                winner.equals(villain) ? 1 : 0,
                winner,
                loser);
        Assertions.assertThat(historyOutput).isEqualToNormalizingWhitespace(historyExpected);
    }

    @Test
    void fightAndDontRecordDefaultHistory() throws Exception {
        var fightOutput = runApp("fight", "-n");
        var ids = parseCharacterIds(fightOutput);
        var hero = HEROES.get(ids.get(0) - 1);
        var villain = VILLAINS.get(ids.get(1) - 1);
        var winner = winner(hero, villain);
        var fightExpected = expectedFightResult(hero, villain, winner);

        Assertions.assertThat(fightOutput).isEqualToNormalizingWhitespace(fightExpected);

        var historyOutput = runApp("history");
        var historyExpected = expectedEmptyHistory();
        Assertions.assertThat(historyOutput).isEqualToNormalizingWhitespace(historyExpected);
    }

    @Test
    void fightAndRecordCustomHistory(@TempDir Path workdir) throws Exception {
        var file = customHistoryFile(workdir);
        var heroWins = 0;
        var villainWins = 0;
        Map<String, String> winner = null;
        Map<String, String> loser = null;

        for (var i = 0; i < 4; i++) {
            var fightOutput = runApp("-f", file, "fight");
            var ids = parseCharacterIds(fightOutput);
            var hero = HEROES.get(ids.get(0) - 1);
            var villain = VILLAINS.get(ids.get(1) - 1);

            winner = winner(hero, villain);
            loser = loser(hero, villain);
            heroWins += winner.equals(hero) ? 1 : 0;
            villainWins += winner.equals(villain) ? 1 : 0;

            var fightExpected = expectedFightResult(hero, villain, winner);

            Assertions.assertThat(fightOutput).isEqualToNormalizingWhitespace(fightExpected);
        }

        var historyOutput = runApp("-f", file , "history");
        var historyExpected = expectedHistory(4, heroWins, villainWins, winner, loser);
        Assertions.assertThat(historyOutput).isEqualToNormalizingWhitespace(historyExpected);

        var defaultHistoryOutput = runApp("history");
        var defaultHistoryExpected = expectedEmptyHistory();
        Assertions.assertThat(defaultHistoryOutput).isEqualToNormalizingWhitespace(defaultHistoryExpected);
    }


    @Test
    void fightAndResetDefaultHistory(@TempDir Path workdir) throws Exception {
        var file = customHistoryFile(workdir);
        runApp("fight");
        runApp("history", "-r");

        var historyOutput = runApp( "history");
        var historyExpected = expectedEmptyHistory();
        Assertions.assertThat(historyOutput).isEqualToNormalizingWhitespace(historyExpected);
    }

    @Test
    void fightAndResetCustomHistory(@TempDir Path workdir) throws Exception {
        var file = customHistoryFile(workdir);
        runApp("-f", file , "fight");
        runApp("-f", file , "history", "--reset");

        var historyOutput = runApp("-f", file , "history");
        var historyExpected = expectedEmptyHistory();
        Assertions.assertThat(historyOutput).isEqualToNormalizingWhitespace(historyExpected);
    }

    String runApp(String... args) throws Exception {
        return SystemLambda.tapSystemErrAndOutNormalized(() -> ApplicationRunner.main(args));
    }


    String customHistoryFile(Path workdir) {
        return workdir.resolve("test-fights.txt").toString();
    }

    static Map<String, String> hero(Long id, String name, String alias, Integer level, String powers) {
        return character("HEROES", id, name, alias, level, powers);
    }

    static Map<String, String> villain(Long id, String name, String alias, Integer level, String powers) {
        return character("VILLAINS", id, name, alias, level, powers);
    }

    static String expectedCharacter(Map<String, String> c) {
        return """
                Team: %s
                Id: #%s
                Name: %s
                Alias: %s
                Level: %s
                Powers: %s
                """.formatted(c.get("team"), c.get("id"), c.get("name"), c.get("alias"), c.get("level"), c.get("powers"));
    }

    static String expectedQuery(int id, List<Map<String, String>> characters) {
        var c = characters.get(id - 1);
        return expectedCharacter(c);
    }

    static String expectedEmptyHistory() {
        return expectedHistory(0, 0, 0, null, null);
    }

    static String expectedLastFight(Map<String, String> winner, Map<String, String> loser) {
        return """
                Winning Team: %s
                Winner: %s (#%s)
                Loser: %s (#%s)
                """.formatted(winner.get("team"), winner.get("name"), winner.get("id"), loser.get("name"), loser.get("id"));
    }

    static String expectedHistory(
            int fights, int heroes, int villains, Map<String, String> winner, Map<String, String> loser) {
        var lastFight = (winner == null || loser == null)
                ? "No fight recorded yet"
                : expectedLastFight(winner, loser);

        return """
                Fight statistics
                ===
                Number of fights: %d
                Heroes won: %d
                Villains won: %d
                ---

                Last recorded fight
                ===
                %s
                """.formatted(fights, heroes, villains, lastFight);
    }

    static List<Integer> parseCharacterIds(String output) {
        var ids = output
                .lines()
                .filter(line -> line.startsWith("Id:"))
                .map(line -> line.substring(line.indexOf('#') + 1))
                .map(String::trim)
                .map(Integer::valueOf)
                .toList();
        if (ids.size() > 2) {
            Assertions.fail("Unable to locate ids of fighting characters in '%s'", output);
        }

        return ids;
    }

    static Map<String, String> winner(Map<String, String> hero, Map<String, String> villain) {
        var hLevel = Integer.parseInt(hero.get("level"));
        var vLevel = Integer.parseInt(villain.get("level"));
        return (hLevel >= vLevel) ? hero : villain;
    }

    static Map<String, String> loser(Map<String, String> hero, Map<String, String> villain) {
        var hLevel = Integer.parseInt(hero.get("level"));
        var vLevel = Integer.parseInt(villain.get("level"));
        return (hLevel >= vLevel) ? villain : hero;
    }

    static String expectedFightResult(Map<String, String> hero, Map<String, String> villain, Map<String, String> winner) {
        var sHero = expectedCharacter(hero);
        var sVillain = expectedCharacter(villain);
        return """
                The fighters
                ===
                %s
                                
                %s
                ---
                                
                The winner is %s (#%s)
                """.formatted(sHero, sVillain, winner.get("name"), winner.get("id"));
    }
}